Source: maxima-sage
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>
Build-Depends: ecl (>= 21.2.1+ds-2), libecl-dev, texinfo, libffi-dev, libgmp3-dev, automake, debhelper ( >=10 ), texlive-latex-recommended, tex-common, python3-dev
Standards-Version: 4.6.0
Homepage: http://maxima.sourceforge.net/
Vcs-Git: https://salsa.debian.org/math-team/maxima-sage.git
Vcs-Browser: https://salsa.debian.org/math-team/maxima-sage

Package: maxima-sage
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: maxima-sage-share
Suggests: maxima-sage-doc
Description: Computer algebra system -- base system
 Maxima is a fully symbolic computation program.  It is full featured
 doing symbolic manipulation of polynomials, matrices, rational
 functions, integration, Todd-coxeter methods for finite group
 analysis, graphing, multiple precision floating point computation.
 It has a symbolic source level debugger for maxima code.  Maxima is
 based on the original Macsyma developed at MIT in the 1970s.  It is
 quite reliable, and has good garbage collection, and no memory leaks.
 It comes with hundreds of self tests.
 .
 The maxima-sage packages are meant to be used together with SageMath.
 They contain the version of Maxima that works together with the
 SageMath version in Debian and use ECL instead of GCL as Lisp compiler.
 To use Maxima by itself, the more complete and up-to-date maxima package
 is recommended.
 .
 This package contains the main executables and base system files.

Package: maxima-sage-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Computer algebra system -- documentation
 Maxima is a fully symbolic computation program.  It is full featured
 doing symbolic manipulation of polynomials, matrices, rational
 functions, integration, Todd-coxeter methods for finite group
 analysis, graphing, multiple precision floating point computation.
 It has a symbolic source level debugger for maxima code.  Maxima is
 based on the original Macsyma developed at MIT in the 1970s.  It is
 quite reliable, and has good garbage collection, and no memory leaks.
 It comes with hundreds of self tests.
 .
 The maxima-sage packages are meant to be used together with SageMath.
 They contain the version of Maxima that works together with the
 SageMath version in Debian and use ECL instead of GCL as Lisp compiler.
 To use Maxima by itself, the more complete and up-to-date maxima package
 is recommended.
 .
 This package contains most of the documentation.

Package: maxima-sage-share
Architecture: all
Depends: maxima-sage (>= ${binary:Version}), ${misc:Depends}
Description: Computer algebra system -- extra code
 Maxima is a fully symbolic computation program.  It is full featured
 doing symbolic manipulation of polynomials, matrices, rational
 functions, integration, Todd-coxeter methods for finite group
 analysis, graphing, multiple precision floating point computation.
 It has a symbolic source level debugger for maxima code.  Maxima is
 based on the original Macsyma developed at MIT in the 1970s.  It is
 quite reliable, and has good garbage collection, and no memory leaks.
 It comes with hundreds of self tests.
 .
 The maxima-sage packages are meant to be used together with SageMath.
 They contain the version of Maxima that works together with the
 SageMath version in Debian and use ECL instead of GCL as Lisp compiler.
 To use Maxima by itself, the more complete and up-to-date maxima package
 is recommended.
 .
 This package contains a set of contributed routines and add-on
 packages.
